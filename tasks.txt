Task #1.

1) Check service description file called 'metadata.xml'. This file contains description of existing OData service and
usually can be accessed via URL, but for this application we will use local version of metadata file. You can see all
the properties and keys available for specific entities in OData service.
2) Check bindings in Worklist and Object views. They already contain some bindings, but not all the fields are presented
in views.
3) Add new column to Table in Worklist view and add all fields available in Projects set to the Object view. All new text
strings for labels should be added to i18n file. All filds should be presented as inputs in SimpleForm in Object view.

Task #2.

1) Check Object view file, Timeframe field have a formatter for the binding. Formatter allows to modify values during
binding.
2) Add formatter function to Team field. It should convert team size to appropriate string: 10 -> "10 Team Members".
3) Make Team field not editable.

Task #3.

1) Add new tab "Team Members" on Object view. This tab should contain some icon chosen from Icon Explorer:
https://openui5beta.hana.ondemand.com/iconExplorer.html
2) Add a table (sap.m.Table) as a content for this new tab. Table should contain information about team members (3 or 4
fields from TeamMembers collection) for the specific project.
NOTES: In metadata there is a collection called TeamMembers. This collection is available as a navigation property for
collection Projects. That means that you can access data containing team members of a specific project by calling:
Projects("[project id should be placed here")/TeamMemberSet. You should simply bind table to this path with a correct
Project Id.

Task #4.

1) Add a popover shoving some additional information for the Team Members table (additional fields which are not shown
in the table). Popover should open when user presses some Button or Link in table row.
NOTES: Use fragment for popover.